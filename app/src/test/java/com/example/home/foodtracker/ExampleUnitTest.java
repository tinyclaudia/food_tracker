package com.example.home.foodtracker;

import android.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;

import org.junit.Test;

import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
    @Test
    public void addition_isCorrect() throws Exception {
        assertEquals(4, 2 + 2);
    }
    @Test
    public void TestPagerAdapter() throws Exception {
        List<android.support.v4.app.Fragment> fragments = new Vector<>();


        ScanCodeAndResultFragment fsc =new ScanCodeAndResultFragment();
        fragments.add(fsc);
        fragments.add(new UserSettingsFragment());

        FragmentManager fragmentManager = new FragmentManager() {
            @Override
            public android.support.v4.app.FragmentTransaction beginTransaction() {
                return null;
            }

            @Override
            public boolean executePendingTransactions() {
                return false;
            }

            @Override
            public android.support.v4.app.Fragment findFragmentById(int id) {
                return null;
            }

            @Override
            public android.support.v4.app.Fragment findFragmentByTag(String tag) {
                return null;
            }

            @Override
            public void popBackStack() {

            }

            @Override
            public boolean popBackStackImmediate() {
                return false;
            }

            @Override
            public void popBackStack(String name, int flags) {

            }

            @Override
            public boolean popBackStackImmediate(String name, int flags) {
                return false;
            }

            @Override
            public void popBackStack(int id, int flags) {

            }

            @Override
            public boolean popBackStackImmediate(int id, int flags) {
                return false;
            }

            @Override
            public int getBackStackEntryCount() {
                return 0;
            }

            @Override
            public BackStackEntry getBackStackEntryAt(int index) {
                return null;
            }

            @Override
            public void addOnBackStackChangedListener(OnBackStackChangedListener listener) {

            }

            @Override
            public void removeOnBackStackChangedListener(OnBackStackChangedListener listener) {

            }

            @Override
            public void putFragment(Bundle bundle, String key, android.support.v4.app.Fragment fragment) {

            }

            @Override
            public android.support.v4.app.Fragment getFragment(Bundle bundle, String key) {
                return null;
            }

            @Override
            public List<android.support.v4.app.Fragment> getFragments() {
                return null;
            }

            @Override
            public android.support.v4.app.Fragment.SavedState saveFragmentInstanceState(android.support.v4.app.Fragment f) {
                return null;
            }

            @Override
            public boolean isDestroyed() {
                return false;
            }

            @Override
            public void registerFragmentLifecycleCallbacks(FragmentLifecycleCallbacks cb, boolean recursive) {

            }

            @Override
            public void unregisterFragmentLifecycleCallbacks(FragmentLifecycleCallbacks cb) {

            }

            @Override
            public android.support.v4.app.Fragment getPrimaryNavigationFragment() {
                return null;
            }

            @Override
            public void dump(String prefix, FileDescriptor fd, PrintWriter writer, String[] args) {

            }

            @Override
            public boolean isStateSaved() {
                return false;
            }
        };
        PagerAdapter pagerAdapter = new PagerAdapter(fragmentManager,fragments);


        assertEquals(2,pagerAdapter.getCount());
    }
    @Test
    public void TestRecyclerViewAdapter() throws Exception {
        List<String> allergens = new ArrayList<>();


        allergens.add("milk");
        allergens.add("soy");

        RecyclerViewAdapter rvAdapter = new RecyclerViewAdapter(allergens);


        assertEquals(2,rvAdapter.getItemCount());
    }


}