package com.example.home.foodtracker;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager.LayoutParams;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;

public class InputAlergenDialogFragment extends DialogFragment {
    EditText txtname;
    Button btnDone;
    static String DialogboxTitle;

    public interface InputAlergenDialogListener {
        void onFinishInputDialog(String inputText);
    }

    //---empty constructor required
    public InputAlergenDialogFragment() {

    }
    //---set the title of the dialog window
    public void setDialogTitle(String title) {
        DialogboxTitle = title;
    }

    public View onCreateView(LayoutInflater inflater,ViewGroup container,Bundle saveInstanceState){

        View view = inflater.inflate(
                R.layout.input_alergen_layout, container);

        //---get the EditText and Button views

        btnDone = (Button) view.findViewById(R.id.btnDone);

        //---event handler for the button
        btnDone.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View view) {
                dismiss();
            }
        });

        ViewPagerFragmentActivity activity = (ViewPagerFragmentActivity)getActivity();
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),android.R.layout.simple_list_item_1, activity.userSettingsFragment.allergens);


        final AutoCompleteTextView acTextView = (AutoCompleteTextView) view.findViewById(R.id.auto);
        acTextView.setThreshold(0);
        acTextView.setAdapter(adapter);


        acTextView.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                acTextView.showDropDown();
                return false;
            }
        });

        acTextView.setOnItemClickListener(new AdapterView.OnItemClickListener() {


            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                String s = (String)adapterView.getItemAtPosition(i);
                InputAlergenDialogListener activity = (InputAlergenDialogListener) getActivity();
                activity.onFinishInputDialog(s);

                //---dismiss the alert
                dismiss();

            }
        });

        //---show the keyboard automatically
        //txtname.requestFocus();
        getDialog().getWindow().setSoftInputMode(
                LayoutParams.SOFT_INPUT_STATE_VISIBLE);

        //---set the title for the dialog
        getDialog().setTitle(DialogboxTitle);

        return view;
    }
}