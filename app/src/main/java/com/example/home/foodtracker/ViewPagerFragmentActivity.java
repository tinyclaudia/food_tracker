package com.example.home.foodtracker;

import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;

import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import java.util.concurrent.TimeUnit;

/**
 * The <code>ViewPagerFragmentActivity</code> class is the fragment activity hosting the ViewPager
 * @author mwho
 */
public class ViewPagerFragmentActivity extends AppCompatActivity implements BarcodeGraphicTracker.BarcodeUpdateListener, InputAlergenDialogFragment.InputAlergenDialogListener {
    /** maintains the pager adapter*/
    private PagerAdapter mPagerAdapter;
    private DatabaseReference myDatabase = FirebaseDatabase.getInstance().getReference();


    Bundle MainSavedInstanceState = null;
    /* (non-Javadoc)
     * @see android.support.v4.app.FragmentActivity#onCreate(android.os.Bundle)
     */
    boolean isOnScanCodeMode = true;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.view_pager);


        if (MainSavedInstanceState != null) {
            isOnScanCodeMode = savedInstanceState.getBoolean("IS_ON_SCAN_CODE_MODE");
        }
        //initialize the pager
        this.initialisePaging();
    }
    @Override
    protected void onPause()
    {
        super.onPause();
    }



    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        // Always call the superclass so it can restore the fragmentView hierarchy
        super.onRestoreInstanceState(savedInstanceState);

        // Restore state members from saved instance
        if (savedInstanceState != null) {
            isOnScanCodeMode = savedInstanceState.getBoolean("IS_ON_SCAN_CODE_MODE");
        }
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();


    }
    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        savedInstanceState.putBoolean("IS_ON_SCAN_CODE_MODE", true);

        MainSavedInstanceState = (Bundle)savedInstanceState.clone();
    }
    @Override
    protected void onStop()
    {

        super.onStop();
    }
    @Override
    protected void onResume()
    {

        super.onResume();
    }

    /**
     * Initialise the fragments to be paged
     */

    ScanCodeAndResultFragment fsc;
    UserSettingsFragment userSettingsFragment;
    private void initialisePaging() {

        List<Fragment> fragments = new Vector<>();


        fsc =new ScanCodeAndResultFragment();
        fsc.isOnScanCode = isOnScanCodeMode;

        userSettingsFragment = new UserSettingsFragment();

        fragments.add(fsc);
        fragments.add(userSettingsFragment);

        this.mPagerAdapter  = new PagerAdapter(super.getSupportFragmentManager(), fragments);
        //
        ViewPager pager = (ViewPager)super.findViewById(R.id.viewpager);

        pager.setAdapter(this.mPagerAdapter);
    }

    AlertDialog dialog;

    public void showInputNameDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        FragmentManager fragmentManager = getSupportFragmentManager();
        InputAlergenDialogFragment inputNameDialog = new InputAlergenDialogFragment();
        inputNameDialog.setCancelable(false);
        inputNameDialog.show(fragmentManager, "Input Dialog");


    }

    @Override
    public void onFinishInputDialog(String inputText) {
        userSettingsFragment.rAdapter.AddItem(inputText);
        userSettingsFragment.allergens.remove( userSettingsFragment.allergens.indexOf(inputText));
    }


    Query products;
    @Override
    public void onBarcodeDetected(Barcode barcode) {
        String code = barcode.displayValue;

        Intent intent = new Intent(ViewPagerFragmentActivity.this, Result.class);
        intent.putExtra("CODE", code);
        startActivity(intent);
    }



}
