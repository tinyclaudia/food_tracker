package com.example.home.foodtracker;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.List;

/**
 * Created by Alek on 15.04.2018.
 */

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ItemViewHolder> {

    private List<String> itemList;
    private int length;


    public String GetElementFromItemList(int index)
    {
        return itemList.get(index);
    }


    public class ItemViewHolder extends RecyclerView.ViewHolder{

        TextView shoppingItem;

        final ItemViewHolder itemViewholder = this;

        public ItemViewHolder(View itemView) {
            super(itemView);
            shoppingItem = (TextView) itemView.findViewById(R.id.step);


        }
    }

    public RecyclerViewAdapter(List<String> itemList ) {
        this.itemList = itemList;
        //listener = _listener;
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_product_item, parent, false);


        return new ItemViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ItemViewHolder holder, int position) {
        String s = itemList.get(position);
        holder.shoppingItem.setText(s);
    }

    public void AddItem( String s) {
        itemList.add(0, new String(s));
        length++;


       notifyDataSetChanged();
    }


    public void RemoveItem( int position) {

        if (position != length) {
            itemList.remove(position);
            length--;
            notifyItemRemoved(position);
        } else {
            itemList.remove(position);
            length--;
            notifyItemRemoved(position);

        }
    }
    @Override
    public int getItemCount() {
        return itemList.size();
    }



}
