package com.example.home.foodtracker;

import java.util.HashMap;
import java.util.List;

/**
 * Created by Home on 2018-05-30.
 */

public class Product {
    public String name;
    public String code;
    public HashMap<String, Allergen> allergens;
}
