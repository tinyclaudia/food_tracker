package com.example.home.foodtracker;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.List;


public class Result extends AppCompatActivity {

    private PagerAdapter mPagerAdapter;
    private DatabaseReference myDatabase = FirebaseDatabase.getInstance().getReference();

    private List<Allergen> allergens;
    Product product;

    boolean isSet = false;

    ImageView iv = null;
    TextView tv = null;
    TextView pName = null;

    Query products;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);

        Intent intent = getIntent();
        String code = intent.getStringExtra("CODE");

        iv = (ImageView)findViewById(R.id.imageView);
        tv = findViewById(R.id.textView);
        pName = findViewById((R.id.productName));

        final List<String> chosenAllergens = readFromInternalStorage();

        iv.setImageDrawable(getResources().getDrawable(R.drawable.ic_priority_high_black_24dp));


        products = myDatabase.child("products").orderByKey().equalTo(code);

        products.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    product = snapshot.getValue(Product.class);
                    pName.setText(product.name);

                    for(int z = 0; z<chosenAllergens.size(); z++) {
                        if (product.allergens.get(chosenAllergens.get(z)).value == true)
                        {
                            iv.setImageDrawable(getResources().getDrawable(R.drawable.ic_close_red_24dp));
                            if(!isSet)
                            {
                                tv.setText("Product contains:" + "\n");
                            }
                            if(product.allergens.get(chosenAllergens.get(z)).name.equals("vegan"))
                            {
                                tv.setText(tv.getText() + "non-vegan" + "\n");
                            }
                            if(product.allergens.get(chosenAllergens.get(z)).name.equals("vegetarian"))
                            {
                                tv.setText(tv.getText() + "non-vegetarian" + "\n");
                            }
                            else
                            {
                                tv.setText(tv.getText() + product.allergens.get(chosenAllergens.get(z)).name + "\n");
                            }
                            isSet = true;
                        }

                    }
                    if(!isSet)
                    {
                        iv.setImageDrawable(getResources().getDrawable(R.drawable.ic_done_black_24dp));
                        tv.setText("Product does not contain chosen allergens");
                        isSet = false;

                    }

                    iv.invalidate();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                    int a =3;
            }
        });

    }
    public ArrayList<String> readFromInternalStorage() {
        ArrayList<String> toReturn = new ArrayList<>();
        // ArrayList<MyItem> s = new ArrayList<>();
        Context ctx = this;
        FileInputStream fis;
        try {
            fis = ctx.openFileInput("o");
            ObjectInputStream oi = new ObjectInputStream(fis);

            toReturn =(ArrayList<String>) oi.readObject();
            for(int i=0;i<toReturn.size();i++)
            {
                String s =(String) oi.readObject();
                toReturn.set(i,s);
            }

            oi.close();
        } catch (FileNotFoundException e) {
            Log.e("InternalStorage", e.getMessage());
        } catch (IOException e) {
            Log.e("InternalStorage", e.getMessage());
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }


        return toReturn;
    }


}
