package com.example.home.foodtracker;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.text.Layout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

public class UserSettingsFragment extends Fragment {
    /** (non-Javadoc)
     * @see Fragment#onCreateView(android.view.LayoutInflater, android.view.ViewGroup, android.os.Bundle)
     */
    public UserSettingsFragment(){}

    View fragmentView;
    List<String> allergens;
    List<String> chosenAllergens;
    RecyclerViewAdapter rAdapter;
    ViewGroup mContainer;
    RecyclerView recyclerView;
    RecyclerView.LayoutManager mLayoutManager;
    ItemTouchHelper itemTouchHelper;

    private DatabaseReference myDatabase = FirebaseDatabase.getInstance().getReference();


    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (container == null) {
            // We have different layouts, and in one of them this
            // fragment's containing frame doesn't exist.  The fragment
            // may still be created from its saved state, but there is
            // no reason to try to create its fragmentView hierarchy because it
            // won't be displayed.  Note this is not needed -- we could
            // just run the code below, where we would create and return
            // the fragmentView hierarchy; it would just never be used.
            return null;
        }



        chosenAllergens = new ArrayList<>();
        mContainer = container;


        //final SwipeController swipeController = new SwipeController();

        //ItemTouchHelper itemTouchhelper = new ItemTouchHelper(swipeController);
        //itemTouchhelper.attachToRecyclerView(recyclerView);


        /*recyclerView.addItemDecoration(new RecyclerView.ItemDecoration() {
            @Override
            public void onDraw(Canvas c, RecyclerView parent, RecyclerView.State state) {
                //swipeController.onDraw(c);
            }
        });*/

        fragmentView = inflater.inflate(R.layout.activity_user_settings, container, false);

        allergens = new ArrayList<>();

        InitObjects();


       // layout.getview
        return (RelativeLayout) fragmentView;
    }

    private void InitObjects() {

        chosenAllergens = readFromInternalStorage();

        InitRecyclerView();
        InitDatabase();
        InitFAB();
    }

    private void InitDatabase() {
        myDatabase.child("allergens").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    Allergen a = snapshot.getValue(Allergen.class);
                    if(!allergens.contains(a.name))
                        allergens.add(a.name);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        for(int i=0; i<chosenAllergens.size(); i++)
        {
            for(int j=0; j<allergens.size(); j++)
            {
                if(chosenAllergens.get(i).equals(allergens.get(j)))
                {
                    allergens.remove(j);
                }
            }
        }
    }

    private void InitFAB() {
        FloatingActionButton floatingButton = fragmentView.findViewById(R.id.fab);
        floatingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Autofill();

                ViewPagerFragmentActivity activity = (ViewPagerFragmentActivity) getActivity();
                activity.showInputNameDialog();
            }


        });
    }

    private void InitRecyclerView() {

        ItemTouchHelper.SimpleCallback simpleItemTouchCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT | ItemTouchHelper.DOWN | ItemTouchHelper.UP) {

            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int swipeDir) {
                final int position = viewHolder.getAdapterPosition();
                {
                    if(!allergens.contains(rAdapter.GetElementFromItemList(position)))
                        allergens.add(rAdapter.GetElementFromItemList(position));
                    rAdapter.RemoveItem(position);
                }
            }
        };

        recyclerView = (RecyclerView) fragmentView.findViewById(R.id.SettingsRecyclerView);
        rAdapter = new RecyclerViewAdapter(chosenAllergens);
        mLayoutManager = new LinearLayoutManager(fragmentView.getContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(rAdapter);
        itemTouchHelper = new ItemTouchHelper(simpleItemTouchCallback);
        itemTouchHelper.attachToRecyclerView(recyclerView);
    }

    private void Autofill() {

        setViewLayout(R.layout.autofill);

        ImageButton ib = fragmentView.findViewById(R.id.back);

        ib.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setViewLayout(R.layout.activity_user_settings);
                InitRecyclerView();
                InitFAB();
            }
        });

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),android.R.layout.simple_list_item_1, allergens);


        final AutoCompleteTextView acTextView = (AutoCompleteTextView) fragmentView.findViewById(R.id.auto);
        acTextView.setThreshold(0);
        acTextView.setAdapter(adapter);


        acTextView.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                acTextView.showDropDown();
                return false;
            }
        });

        acTextView.setOnItemClickListener(new AdapterView.OnItemClickListener() {


            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                String s = (String)adapterView.getItemAtPosition(i);
                AddAllergenToList(s);
            }
        });

    }



    private void AddAllergenToList(String s) {
        InitRecyclerView();
        InitFAB();
        rAdapter.AddItem(s);
        allergens.remove(allergens.indexOf(s));
    }

    private void ItemSelected(View view) {


    }

    public String GetClassName()
    {
        Class c = this.getClass();
        return c.getCanonicalName();
    }

    private void setViewLayout(int id) {
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        fragmentView = inflater.inflate(id, mContainer, false);

        ViewGroup rootView = (ViewGroup) getView();
        rootView.removeAllViews();
        rootView.addView(fragmentView);
    }


    public void saveToInternalStorage(List<String> itl) {
        try {
            Context ctx = getContext();
            FileOutputStream fos = ctx.openFileOutput("o", Context.MODE_PRIVATE);
            ObjectOutputStream of = new ObjectOutputStream(fos);

            of.writeObject(itl);
            for(int i=0;i<itl.size();i++)
            {
                of.writeObject(itl.get(i));
            }


            of.flush();
            of.close();
            fos.close();
        }
        catch (Exception e) {
            Log.e("InternalStorage", e.getMessage());
        }
    }

// Insert the new row, returning the primary key value of the new row



    public ArrayList<String> readFromInternalStorage() {
        ArrayList<String> toReturn = new ArrayList<>();
        Context ctx = getContext();
        FileInputStream fis;
        try {
            fis = ctx.openFileInput("o");
            ObjectInputStream oi = new ObjectInputStream(fis);

            toReturn =(ArrayList<String>) oi.readObject();
            for(int i=0;i<toReturn.size();i++)
            {
                String s =(String) oi.readObject();
                toReturn.set(i,s);
            }
            oi.close();
        } catch (FileNotFoundException e) {
            Log.e("InternalStorage", e.getMessage());
        } catch (IOException e) {
            Log.e("InternalStorage", e.getMessage());
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return toReturn;
    }

    @Override
    public void onPause() {
        super.onPause();
        saveToInternalStorage(chosenAllergens);

    }

    @Override
    public void onResume() {
        super.onResume();

        chosenAllergens = readFromInternalStorage();

    }



}
